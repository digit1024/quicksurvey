angular.module("App" ,['ui.router']).config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/survey");
  $stateProvider
    .state('survey', {
      url: "/survey",
      templateUrl: "/view/survey.html",
      controller: "Survey"
    })
    .state('results', {
      url: "/results",
      controller: "Results" , 
      templateUrl: "view/results.html"
    });
    
})
.controller("MainController" , ["$scope"  , "$window",  function($scope, $window){
			
  		$scope.SaveState=  function (model) {
            $window.localStorage["model"] = angular.toJson(model);
        },

        $scope.RestoreState =  function () {
             return  angular.fromJson($window.localStorage["model"]);
        }
        $scope.data = $scope.RestoreState();
        $scope.data = $scope.data || {"good": 0 ,"bad":0  , "ok": 0};

        $scope.reset= function(){
        	console.log("reset");
			$scope.data = {"good": 0 ,"bad":0  , "ok": 0};       
			
        }

}] )
.controller("Survey" , ["$scope" , "$timeout",  function($scope, $timeout){
	$scope.showingThankYou = false; 
	$scope.score =function(result){
		$scope.data[result] = $scope.data[result] + 1;
		console.log($scope.data);
		$scope.SaveState($scope.data);	
		$scope.showingThankYou = true; 	
			$scope.SaveState($scope.data);	
			    $timeout(function() {
        		$scope.showingThankYou = false; 	
    			}, 2000);
	}
}] )
.controller("Results" , ["$scope" ,  function($scope){}] )